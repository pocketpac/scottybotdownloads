#Instructions Below

All downloads related to Scottybot will be found here.

[ScottyGUI.Jar](http://fdn.redstone.tech/scottybot/scottygui/ScottyGUI.jar) is for people who use non-Windows machines or are more advanced users. Must use Java 8.

[ScottyGUI-Installer.exe](http://fdn.redstone.tech/scottybot/scottygui/ScottyGUI-Installer.exe) is for most Windows users. Comes pre-packaged with everything needed to get going.

##ScottyGUIUpdater.jar is useless to you directly. It is involved in the auto-update process.